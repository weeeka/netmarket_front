import React from 'react';
import MoneyClaim from "./MoneyClaim";
import "./Money.css"


export default ({ data }) => {
    if (!data) { return (<p>Loading...</p>); }
    const moneyClaims = data.map((moneyClaim) => {
        return (<MoneyClaim moneyClaim={moneyClaim} />);
    });
    return (
        <div className="window">
        <table >
            <thead>
            <tr>
                <th>Дата подачи</th>
                <th>Дата подтверждения</th>
                <th>Сумма</th>
            </tr>
            </thead>

            <tbody>
            {moneyClaims}
            </tbody>
        </table>
        </div>
    );
};