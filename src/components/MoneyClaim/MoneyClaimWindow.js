import React, {Component} from 'react';
import Button from 'react-bootstrap/Button'
import {PostData} from "../../services/PostData";
import MoneyClaimList from "./MoneyClaimList";
import {GetData} from "../../services/GetData";
import {PutData} from "../../services/PutData";
import "./Money.css"


class MoneyClaimWindow extends Component {

    constructor(){
        super();
        this.state={
            amount: '',
            moneyClaims: null,
            lastClaim: null,
            message: null
        };
        this.onChange = this.onChange.bind(this);
        this.loadData();

    }


    loadData(){
        GetData('moneyClaim/myClaims').then(moneyClaims => {
            this.setState({
                moneyClaims: moneyClaims,
                lastClaim: this.getLastClaim(moneyClaims),
                message: ""
            });
        }).catch(() => {
            this.setState({
                moneyClaims: [],
                message: "Заявки отсутствуют"
            })
        });

    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    changeClaim(){
        let moneyClaim = this.state.lastClaim
        moneyClaim.amount = this.state.amount
        PutData('moneyClaim?moneyClaimId='+this.state.lastClaim.id, moneyClaim)
            .then(()=>{this.loadData()})

    }

    getLastClaim(moneyClaims){
        return  moneyClaims.find(item=>item.status === null)
    }

    buttonTag(){
        if(this.state.lastClaim){
         return <Button className="butn" variant="primary" size="sm" onClick={()=>this.changeClaim()}>Изменить</Button>
        }
    else{
          return <Button className="butn" variant="primary" size="sm" onClick={()=>this.send()}>Сохранить</Button>
        }
    }

    send() {
        if(!this.state.lastClaim){
        PostData('moneyClaim/'+sessionStorage.getItem('username'), {amount: this.state.amount})
            .then(()=>{this.loadData()});
        alert("Ваша заявка будет рассмотрена в ближайшее время");
        }
        else {
            alert("Заявка уже была отправлена и будет рассмотрена в ближайшее время")
        }
    }
    getMoneyClaimList(){
        if(!this.state.message){
            return (<MoneyClaimList className="window" data={this.state.moneyClaims}/>)
        }
        else {
            return (this.state.message)
        }
    }



    render(){
        return (
        <div>
            <div className="input-sum">        Введите сумму:   </div>   
             <div className="window"><input name="amount" onChange={this.onChange}/> {this.buttonTag()} </div>
            <br/><br/>
            {this.getMoneyClaimList()}
        </div>
        );
    }
}export default MoneyClaimWindow