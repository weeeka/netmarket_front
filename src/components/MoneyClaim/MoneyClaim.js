import React from "react";

export default ({ moneyClaim}) => {
       return (
        <tr>
            <td>{moneyClaim.creationDate}</td>
            <td>{moneyClaim.reviewDate}</td>
            <td>{moneyClaim.amount}</td>
        </tr>
    );
};

