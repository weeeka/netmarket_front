import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Label } from "react-foundation";
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import { PostNData } from "../../services/PostNData";
import { GetData } from "../../services/GetData";

import "./Sale.css"

class Sale extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
      categories: [],
      price: null,
      name: null,
      text: null,
      category_name: null,
      quantity: null,
      category_id: null,
      client_id: null,
      imageURL: null,
      code: null
    };
    this.loadData();
    this.handleChange = this.handleChange.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
    document.location.href = "http://localhost:3000/home";
  }

  loadData() {
    GetData("client/" + sessionStorage.getItem("username")).then(client => {
      this.setState({ client_id: client.id });
      GetData("category").then(categories => {
        this.setState({
          categories: categories,
          category_id: categories[0].id
        });
      });
    });
  }

  sell() {
    let product = {
      name: this.state.name,
      price: this.state.price,
      text: this.state.text,
      quantity: this.state.quantity,
      category_id: this.state.category_id,
      client_id: this.state.client_id,
      imageURL: this.state.imageURL,
      code: this.state.code,
      valide: false
    };

    PostNData("product", product)
      .then(product => {
        this.setState({
          showModal: true
        });
      })
      .catch(error => {
        alert("Заполните все необходимые поля");
      });
  }

  CategoryOption() {
    this.state.categories.map((category) => {
        return (<option>{category.name}</option>);
         })
  }

  render() {
    return (
        <div style={{margin : '2rem 0'}}>
       <div className="center text-header">Параметры </div><br/><br/> 

      <Form>
        <Form.Group>
          <Form.Label className="center">Название товара</Form.Label>
          <Form.Control className="center" name = 'name' as="input" value={this.state.name} onChange={e => this.handleChange(e)}/>
          <Form.Text  className="center text-muted">
                Например «Gulman 5» или «Minecraft»
            </Form.Text>
        </Form.Group>

        <Form.Group>
          <Form.Label className="center">Цена</Form.Label>
          <Form.Control className="center" name = 'price' value={this.state.price}  onChange={e => this.handleChange(e)}/>
        </Form.Group>

        <Form.Group>
        <Form.Label className="center" >Категория:</Form.Label><br/>
           <select className="center" name = "category_id"  onChange = {e => this.handleChange(e)}>
               {this.state.categories.map((category) => {
                   return (<option value={category.id}>{category.name}</option>);
               })
               }
           </select>
        </Form.Group>
        
        <Form.Group>
          <Form.Label className="center">Описание:</Form.Label>
          <Form.Control className="center" name = 'text' as="textarea" rows="3" value={this.state.text} onChange={e => this.handleChange(e)}/>
        </Form.Group>   

        <Form.Group>
          <Form.Label className="center" >Количество: </Form.Label>
          <Form.Control className="center" name = 'quantity' value={this.state.quantity} onChange={e => this.handleChange(e)}/>
        </Form.Group>

        <Form.Group>
          <Form.Label className="center">Изображение: </Form.Label>
          <Form.Control className="center" name = 'imageURL' value={this.state.imageURL}  onChange={e => this.handleChange(e)}/>
          <Form.Text  className="center text-muted">
                Ссылка на изображение
            </Form.Text>
        </Form.Group>

        <div className="center text-header">Код </div><br/><br/> 


        <Form.Group>
          <Form.Label className="center" >Код активации: </Form.Label>
          <Form.Control className="center" name = 'code' value={this.state.code}  onChange={e => this.handleChange(e)}/>
            <Form.Text  className="center text-muted">
                Мы никому не сообщим ваш код, кроме покупателя
            </Form.Text>
        </Form.Group>

        <Button className="center" variant="success" onClick={()=>this.sell()}>Выставить на продажу</Button><br/>

        <Modal show={this.state.showModal} onHide={this.handleCloseModal} animation={true}  centered>
          {/* <Modal.Header closeButton>
            <Modal.Title>Запрос на пополнение баланса</Modal.Title>
          </Modal.Header> */}

          <Modal.Body>
              
          Товар успешно выставлен на продажу
            </Modal.Body>


          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModal}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>



      </Form>
      </div>
    );
  }
}

export default Sale;
