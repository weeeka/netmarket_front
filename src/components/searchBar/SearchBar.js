import React from 'react';
import "./searchBar.css";
import {GetData} from "../../services/GetData";

export default ({type, update}) => {

    const dataSearch = e => {
        const value = e.target.value.toLowerCase();

        GetData(type+value).then( search=>
        {update({
                search: search,
                line: value
            })}
        );

    };

    return (
        <div>
        <div className="searchbar form-group">
            <input
                type="text"
                className="form-control"
                placeholder="Искать игру..."
                onChange={dataSearch}
            />
        </div>
        </div>
    );
};