import React, { Component } from "react";
import { GetData } from "../../services/GetData";
import ProductList from "../Products/ProductList";
import CategoryList from "../categories/CategoryList";
import SearchBar from "../searchBar/SearchBar";
import ReactPaginate from 'react-paginate';
import Pagination from 'react-bootstrap/Pagination'
import PageItem from 'react-bootstrap/PageItem'
import Nav from 'react-bootstrap/Nav'

import "./Home.css";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      categories: null,
      term: "",
      products: null,
      totalPages: null,
      currentNumPage: 0,
      pageSize: 10,
      activeCategories:null,
      activeSearch: false,
      searchLine: null
    };
    this.loadCategories();
    this.loadProducts();
    // this.RunOutOfProduct();
    // this.RunOutOfProduct = this.RunOutOfProduct.bind(this);
  }
  filteredByCategory(config) {
    this.setState({
      activeCategories: config.active,
      currentNumPage: 0,
      searchLine: null,
      activeSearch: false
    },()=>{
    if(this.state.activeCategories===null){
      this.loadProducts()
    }
    else {
    this.loadProductsByActiveCategory();
    }})
  }

  loadCategories() {
    GetData("category").then(categories => {
      this.setState({
        categories: categories
      });
    });}

  loadProducts() {
    GetData("product?page="+this.state.currentNumPage+"&size="+this.state.pageSize).then(page => {
      this.setState({
        products: page.content,
        totalPages: page.totalPages
      });
    });
  }

  loadProductsByActiveCategory(){
  GetData("product/findByCategoryId/"+ this.state.activeCategories+"?size="+this.state.pageSize + "&page="+this.state.currentNumPage).then(page=>{
    this.setState({
      products:page.content,
      totalPages: page.totalPages
    })
  })
  }

  pageChange = (currentPage)=>{

    this.setState({
      currentNumPage: currentPage.selected}, () =>{
      if(this.state.activeCategories!==null){
        this.loadProductsByActiveCategory()
      }else if(this.state.activeSearch){
        GetData("product/search/?page="+ this.state.currentNumPage+ "&size=10&name=" + this.state.searchLine).then(data=>{
            this.setState({
              products: data.content,
            })}
        )
      }
      else{
      this.loadProducts()}
    });
    };

  updateData=(data)=>{
    this.setState({
      products: data.search.content,
      totalPages: data.search.totalPages,
      activeSearch: true,
      searchLine: data.line,
      activeCategories: null
    });
  };

  

  render() {
    if (sessionStorage.length !== 0) {
      if (sessionStorage.getItem("role") !== "[ROLE_ADMIN]") {
        return (
          <div>
   
            <div className="row-elems">
            <div className="left-menu">
              <div className="left-menu-narrow">
            <div className="search-bar"><SearchBar  type={"product/search/?size=10&name=" } update={this.updateData.bind(this)} /></div>
            <div className="menu-border"><CategoryList data={this.state.categories} update={this.filteredByCategory.bind(this)}/></div>
            </div>
            </div>

            

            <div className="products">
              
              <ProductList data={this.state.products} />

              <ReactPaginate pageCount={this.state.totalPages}
                           pageRangeDisplayed={5}
                           marginPagesDisplayed={1}
                           onPageChange={this.pageChange}
                           forcePage = {this.state.currentNumPage}
                           breakClassName={'page-item'}
                           breakLinkClassName={'page-link'}
                           containerClassName={'pagination'}
                           pageClassName={'page-item'}
                           pageLinkClassName={'page-link'}
                           previousClassName={'page-item'}
                           previousLinkClassName={'page-link'}
                           nextClassName={'page-item'}
                           nextLinkClassName={'page-link'}
                           activeClassName={'active'}
                            />
                            
              </div>
            </div>


            
          </div>
        );
      }
      else return (
        <div className="vert">
      <h1 className="hello">
      <span class="color-red">H</span>   
      <span class="color-green">E</span>   
      <span class="color-yellow">L</span>   
      <span class="color-red">L</span>   
      <span class="color-green">O</span>   
      </h1>
      <h1 className="hello">
      <br/>
      <span class="color-green">A</span>
      <span class="color-yellow">D</span>
      <span class="color-red">M</span>
      <span class="color-green">I</span>
      <span class="color-yellow">N</span>
      <span>!</span>
      </h1>
      <h3 className="hello">This is an admin page</h3>
      <h4 className="hello">For admins</h4>
      </div>)
    }
  }
}
export default Home;
