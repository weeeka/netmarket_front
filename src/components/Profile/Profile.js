import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import { NavLink } from "react-router-dom";
import { GetData } from "../../services/GetData";
import ReactModal from "react-modal";
import "./Profile.css";
import MoneyClaimWindow from "../MoneyClaim/MoneyClaimWindow";
import Link from "react-router-dom/Link";
import Modal from 'react-bootstrap/Modal'

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      address: "",
      showModal: false,
      imageURL: null,
      
    };
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.loadData();
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  loadData() {
    GetData("client/" + sessionStorage.getItem("username")).then(client => {
      if (
        client.firstName == null &&
        client.lastName === null &&
        client.address === null
      ) {
        this.setState({
          firstName: "Имя",
          lastName: " Фамилия",
          address: " aaa@example.com",
          bonus: client.bonus,
          imageURL: client.imageURL,
          
        });
      } else {
        this.setState({
          firstName: client.firstName,
          lastName: client.lastName,
          address: client.address,
          bonus: client.bonus,
          imageURL: client.imageURL
        });
      }
    });
  }

  getUserImage() {
    if (!this.state.imageURL) {
      return (
        <img
          src="https://www.saludyciclovital.com/wp-content/uploads/2019/05/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg"
          className="round"
        />
      );
    } else {
      return <img src={this.state.imageURL} className="round" />;
    }
  }

  clientRole(){
    if (sessionStorage.getItem('role') == '[ROLE_ADMIN]')
    return (<div className="user-role">Админ</div>)
  }

  render() {
    return (
      <div>
        <br />
        <div className="profilePic">{this.getUserImage()}</div>
        <br />

        <div>{this.clientRole()}</div>

        <div className="profileName">
          {this.state.firstName} {this.state.lastName}{" "}
        </div>

        <div className="username">
          {" "}
          @{sessionStorage.getItem("username")} • {this.state.address}
        </div>

        <NavLink className="infochange" to="history">
          История заказов
        </NavLink>

        <NavLink className="infochange" to="profileEdit">
          Редактировать профиль
        </NavLink>
        <br />

        <div className="money">
          <div className="money-text">На Вашем счёте: {this.state.bonus}</div>
          <Button
            className="money-button"
            variant="info"
            onClick={() => this.handleOpenModal()}
            size="sm"
          >
            Пополнить баланс
          </Button>
        </div>

        

        <Modal show={this.state.showModal} onHide={this.handleCloseModal} animation={true}  centered>
          <Modal.Header closeButton>
            <Modal.Title>Запрос на пополнение баланса</Modal.Title>
          </Modal.Header>

          <Modal.Body>
              
              <MoneyClaimWindow/>
            </Modal.Body>


          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModal}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>

      </div>
    );
  }
}
export default Profile;
