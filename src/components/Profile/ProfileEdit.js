import React, {Component} from 'react'
import {Label} from "react-foundation";
import {NavLink} from "react-router-dom";
import {PutData} from "../../services/PutData";
import {GetData} from "../../services/GetData";
import './Profile.css'
import ReactModal from "react-modal";
import Button from "antd/es/button";

class ProfileEdit extends Component {
    constructor() {
        super();
        this.state={
            showModal: false,
            firstName: ' ',
            lastName: ' ',
            address: ' ',
            enabled: true,
            imageURL:' '
        }
        this.loadData();
        this.handleChange = this.handleChange.bind(this);
     }

    loadData() {
        GetData("client/" + sessionStorage.getItem("username")).then(client => {
           this.setState({
                firstName: client.firstName,
                lastName: client.lastName,
                address: client.address,
                imageURL: client.imageURL
           });
        });
    }

    handleChange = (e) => {
        this.setState({[e.target.name]:e.target.value})
    };

    handleOpenModal(){
        this.setState({showModal: true});
    }

    handleCloseModal(){
        this.setState({showModal: false});
        document.location.href = "http://localhost:3000/profile";
    }


    save(){
        PutData('client/'+ sessionStorage.getItem("username"), this.state).then(client =>{
            this.setState({showModal: true});
        })
    }




    render() {
        return(
            <div className="aaaaaaaa">
                <h3>Редактировать профиль</h3><br/>
               <Label>First Name:</Label><br/>
               <input name = 'firstName'  value={this.state.firstName}
                      onChange={e => this.handleChange(e)}  /><br/><br/>
               <Label>Last Name:</Label><br/>
               <input name = 'lastName'   value={this.state.lastName} onChange={e => this.handleChange(e)}/><br/><br/>
               <Label>Address:</Label><br/>
               <input name = 'address'  value={this.state.address} onChange={e => this.handleChange(e)}/><br/><br/>
                <Label>Image URL:</Label><br/>
               <input name = 'imageURL'  value={this.state.imageURL} onChange={e => this.handleChange(e)}/><br/>
                <Button onClick={()=> this.save()}>Ok</Button><br/>
                <NavLink to="/profile">Back</NavLink>

                <ReactModal isOpen = {this.state.showModal}>
                    <Label onMouseOver={()=> this.handleCloseModal()}>Данные успешно изменены</Label>
                </ReactModal>
            </div>)
    }
}
export default ProfileEdit;

