import React from "react";
import Product from "./Product";
import "./Product.css";
import ListGroup from 'react-bootstrap/ListGroup'
// Необходимо импортировать и написанный нами ранее компонент UserData

export default ({ data }) => {
  if (!data) {
    return <p>Loading...</p>;
  }
  const products = data.map(product => {
     if (product.existence && product.valide)
    return <Product product={product}/>;
  });

  return (
    <div>
      
    <ListGroup variant="flush">
      {products}
    </ListGroup>
    
  </div>  
  );
};
