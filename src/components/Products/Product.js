import React, { Component } from "react";
import {Add} from "../Cart/AddProduct";
import Button from 'react-bootstrap/Button'
import { GetData } from "../../services/GetData";
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'

class Product extends Component{
  constructor(props) {
    super(props);
    this.state = {
      cond: false,
      sellerName: ""
    };
    this.showSellerName(this.props.product);

     this.RunOutOfProduct(this.state.cond);
     this.RunOutOfProduct = this.RunOutOfProduct.bind(this);
     this.RunOutCallBack = this.RunOutCallBack.bind(this);
  }
  
  
  RunOutOfProduct(value){
    if(value) return <div style={{color: "red"}}>Больше нельзя добавить в корзину этот товар</div>
    else return <div> </div>
  }

  RunOutCallBack(value){
    this.setState({
      cond: value
    });
  }

  showSellerName(product) {
    GetData('client/findById/' + product.client_id).then(client => {     
        this.setState({
            sellerName: client.username
        });   
      })
  }

  render(){
    return (
      <ListGroup.Item> 

          <div className="row-product-elems">
              <div>
                <div className="product-picture">  <img src={this.props.product.imageURL} className="img"/> </div>
                <br/>
                <Button className="product-btn"variant="outline-info" size="sm" onClick={()=>  Add(this.props.product, this.RunOutCallBack)}>В корзину</Button> 
              </div>

              <div className="product-body">
                <div className="product-text-header">{this.props.product.name} • {this.props.product.quantity} шт. </div>
                <div className="product-price"><Badge variant="success"> {this.props.product.price} ед.</Badge></div>
               
                <p className="product-description">{this.props.product.text}</p>
                <p></p>              
                
                {this.RunOutOfProduct(this.state.cond)}
              </div>
          </div>
    
        </ListGroup.Item>
    );
    }
} export default Product