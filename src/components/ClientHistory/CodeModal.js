import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from 'react-bootstrap/Modal'
import "./ClientHistory.css";

class CodeModal extends Component {
    render(){
        return(
            <Modal
            show={this.props.showModalToCode}
            onHide={this.props.handleCloseModalToCode}
            animation={true}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title>Код активации</Modal.Title>
            </Modal.Header>

            <Modal.Body>
            {this.props.code}
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={this.props.handleCloseModalToCode}>
                Закрыть
              </Button>
            </Modal.Footer>
          </Modal>
        )
    }
} export default CodeModal