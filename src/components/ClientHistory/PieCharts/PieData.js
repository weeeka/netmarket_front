import React, { Component } from "react";
import { Pie } from "react-chartjs-2";

export default ({ categories, categoryNames}) => {

categories.sort(function(a, b) {
    return a.id - b.id;
  });

  //получаем объект состоящмй из пар "категория: количество"
  let catFreq = categories.reduce(function(allCats, cat) {
    if (cat in allCats) {
      allCats[cat]++;
    } else {
      allCats[cat] = 1;
    }
    return allCats;
  }, {});
  //например { '1': 1, '2': 2 }
  //в кавычках айдишники категории, то есть продуктов с айди категории 1 - 1 шт
  //редьюс возвращает тока объект, поэтому из объекта надо получить обратно массив

  //теперь получаем массив частот, например [1, 2]
  catFreq = Object.values(catFreq);

  let colors = []

  //теперь можно задать цвета для каждой категории для отображения ее на диаграмме
  //задаем рандомный цвет
  for (let i = 0; i < catFreq.length; i++) {
    colors[i] = "#" + Math.floor(Math.random() * 16777215).toString(16);
  }


  //теперь categoryNames и categories взаимнооднозначно соответствуют, т.к. categories был изначально отсортирован

  //массив данных для отрисовки диаграммы, он должен быть именно таким
  //источник(пример): https://github.com/jerairrest/react-chartjs-2/blob/master/example/src/components/pie.js
  const data = {
    labels:  categoryNames,
    datasets: [
      {
        data: catFreq,
        backgroundColor: colors,
        hoverBackgroundColor: colors
      }
    ]
  };
  return (
    <Pie data={data} />
  )
}