import React, { Component } from "react";
import { Pie } from "react-chartjs-2";


import PieData from "./PieData.js"
import {GetData} from "../../../services/GetData";

class PurchasePie extends Component {
  //export default ({ orders}) => {
  constructor(props) {
    super(props);
    this.state = {
      //это будет массив частот каждой категории в конце (сколько товаров какой категории купил юзер)
      categories: [],
      //это массив имен категорий
      categoryNames: []
      //цвета для диаграммы
      //colors:[],
    };
    this.loadData();
    //this.loadCategories()
    this.loadData = this.loadData.bind(this);
    //this.loadCategories = this.loadCategories.bind(this);
  }

  loadData() {
    let orders = this.props.orders;
    let catTemp = [];
    let catNamesTemp = [];
    //проходим по всем ордер_айтемам юзера и сохраняем айди категории его продукта
    for (let i = 0; i < orders.length; i++) {
      GetData("orderItem/" + orders[i].id).then(items => {
        for (let j = 0; j < items.length; j++) {
          for (let k = 0; k < items[j].quantity; k++){
          GetData("product/" + items[j].product_id).then(product => {
            GetData("category/findById/" + product.category_id).then(ctgr => {
            catTemp.push(product.category_id);
            //catNamesTemp.add(ctgr.name)
            if (!catNamesTemp.includes(ctgr.name)) catNamesTemp.push(ctgr.name)
            this.setState({
              categories: catTemp,
              categoryNames:catNamesTemp
            });
          })
            //categories.push(product.category_id);
          });
        }
      }
      });
    }
  }

  //получаем имена категорий, чтобы подписыввать ими диаграмму
  // loadCategories() {
  //   GetData("category").then(categories => {
  //     let catTemp = [];
  //     for (let i = 0; i < categories.length; i++) {
  //       catTemp.push(categories[i].name);
  //       this.setState({
  //         categoryNames: catTemp
  //       });
  //     }
  //   });
  // }



  render() {
    return (
      <PieData
        categories={this.state.categories}
        categoryNames={this.state.categoryNames}
      />
    );
  }
}
export default PurchasePie;
