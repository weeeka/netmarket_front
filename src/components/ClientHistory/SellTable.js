import React, { Component } from "react";
import "./ClientHistory.css";

class SellTable extends Component {
    render(){
        return(
            <div>
          <table >
            <tbody>
              {this.props.sellItem.map(product => {
                
                  return (
                    <tr height="150" className="order">
                      <td className="info">                       
                         {product.name} <br />                       
                      </td>

                      <td >                                               
                          <div className="info qty">{product.quantity} шт.</div>                     
                      </td>

                      <td className="cost">
                        Стоимость: <br />{" "}
                        <div className="digits-green">{product.price * product.quantity}</div>
                      </td>
                    </tr>
                  );                
              })}
            </tbody>
          </table>

          </div>
        )
    }
} export default SellTable