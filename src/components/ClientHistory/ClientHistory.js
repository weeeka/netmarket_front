import React, { Component } from "react";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

import OrderModal from "./OrderModal.js";
import CodeModal from "./CodeModal.js";
import HistoryTable from "./HistoryTable.js";
import SellTable from "./SellTable.js";
import PurchasePie from "./PieCharts/PurchasePie.js";
import SellPie from "./PieCharts/SellPie.js";

import "./ClientHistory.css";
import { GetData } from "../../services/GetData";

class ClientHistory extends Component {
  constructor() {
    super();
    this.state = {
      client_id: null,
      orders: [],
      orderItem: [],
      sellItem: [],
      showModalToOrder: false,
      showModalToCode: false,
      showModalToPurPie: false,
      showModalToSellPie: false,
      code: null
    };
    this.loadData();
    this.loadData = this.loadData.bind(this);
    this.handleOpenModalToOrder = this.handleOpenModalToOrder.bind(this);
    this.handleCloseModalToOrder = this.handleCloseModalToOrder.bind(this);
    this.handleOpenModalToCode = this.handleOpenModalToCode.bind(this);
    this.handleCloseModalToCode = this.handleCloseModalToCode.bind(this);
    this.handleOpenModalToPurPie = this.handleOpenModalToPurPie.bind(this);
    this.handleCloseModalToPurPie = this.handleCloseModalToPurPie.bind(this);
    this.handleOpenModalToSellPie = this.handleOpenModalToSellPie.bind(this);
    this.handleCloseModalToSellPie = this.handleCloseModalToSellPie.bind(this);
  }

  handleOpenModalToOrder() {
    this.setState({ showModalToOrder: true });
  }

  handleCloseModalToOrder() {
    this.setState({ showModalToOrder: false });
  }

  //pur - покупка (purchase)
  handleOpenModalToPurPie() {
    this.setState({ showModalToPurPie: true });
  }

  handleCloseModalToPurPie() {
    this.setState({ showModalToPurPie: false });
  }

  handleOpenModalToSellPie() {
    this.setState({ showModalToSellPie: true });
  }

  handleCloseModalToSellPie() {
    this.setState({ showModalToSellPie: false });
  }

  handleOpenModalToCode() {
    this.setState({ showModalToCode: true });
  }

  handleCloseModalToCode() {
    this.setState({ showModalToCode: false });
  }

  loadData() {
    GetData("client/" + sessionStorage.getItem("username")).then(client => {
      GetData("order/findAll/" + client.id).then(orders => {
        this.setState({
          orders: orders,
          client_id: client.id
        });
        //загружаем продажи
        GetData("orderItem").then(orderItemForSale => {
          let sellItemNew = [];
          for (let i = 0; i < orderItemForSale.length; i++) {
            GetData("product/" + orderItemForSale[i].product_id).then(
              product => {
                if (product.client_id === this.state.client_id) {
                  product.quantity = orderItemForSale[i].quantity;
                  sellItemNew.push(product);
                  this.setState({
                    sellItem: sellItemNew
                  });
                }
              }
            );
          }
        });
      });
    });
  }

  showOrder = order => {
    GetData("orderItem/" + order.id).then(orderItem => {
      this.setState({
        orderItem: orderItem,
        showModalToOrder: true
      });
    });
  };

  showCode = product_id => {
    GetData("product/" + product_id).then(product => {
      this.setState({
        code: product.code,
        showModalToCode: true
      });
    });
  };

  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <div className="inline">
          <br />
          <div className="text-header">Заказы</div>
          <br />
          <br />

          <div>
            <Button
              variant="outline-info"
              onClick={this.handleOpenModalToPurPie}
            >
              Смотреть статистику покупок
            </Button>
          </div>

          <br />

          <HistoryTable orders={this.state.orders} showOrder={this.showOrder} />
        </div>

        <div className="sell-table inline">
          <div className="text-header">Продажи</div>
          <br />
          <br />
          <div>
          <Button
              variant="outline-info"
              onClick={this.handleOpenModalToSellPie}
            >
              Смотреть статистику продаж
            </Button>
            </div>
            <br />

          <SellTable sellItem={this.state.sellItem} />
        </div>

        <OrderModal
          showModalToOrder={this.state.showModalToOrder}
          handleCloseModalToOrder={this.handleCloseModalToOrder}
          orderItem={this.state.orderItem}
          showCode={this.showCode}
        />

        <CodeModal
          showModalToCode={this.state.showModalToCode}
          handleCloseModalToCode={this.handleCloseModalToCode}
          code={this.state.code}
        />



        {/* диаграмма купленных игр по жанрам */}



        <Modal
          show={this.state.showModalToPurPie}
          onHide={this.handleCloseModalToPurPie}
          animation={true}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Статистика купленных игр по жанрам</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <PurchasePie orders={this.state.orders} loadData={this.loadData} />
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModalToPurPie}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>


      {/* диаграмма проданных игор */}

      <Modal
          show={this.state.showModalToSellPie}
          onHide={this.handleCloseModalToSellPie}
          animation={true}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Статистика проданных игр по жанрам</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <SellPie clientId={this.state.client_id} />
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModalToSellPie}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
export default ClientHistory;
