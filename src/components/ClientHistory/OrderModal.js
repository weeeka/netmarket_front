import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from 'react-bootstrap/Modal'
import "./ClientHistory.css";
class OrderModal extends Component {

render() {
    return(
        <Modal
            show={this.props.showModalToOrder}
            onHide={this.props.handleCloseModalToOrder}
            animation={true}
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title>Состав заказа</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              
              <div className="window">
                <table className="orderItem-list">
                  <thead>
                    <tr>
                      <th>Название продукта</th>
                      <th>Цена</th>
                      <th>Количество</th>
                      <th>Код активации</th>
                    </tr>
                  </thead>

                  <tbody>
                    {this.props.orderItem.map(productToOrder => {
                      return (
                        <tr>
                          <td>{productToOrder.name}</td>
                          <td>{productToOrder.price}</td>
                          <td>{productToOrder.quantity}</td>
                          <button onClick={()=>this.props.showCode(productToOrder.product_id)}>Посмотреть код активации</button>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={this.props.handleCloseModalToOrder}>
                Закрыть
              </Button>
            </Modal.Footer>
          </Modal>
    )
}

} export default OrderModal