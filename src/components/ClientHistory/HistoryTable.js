import React, { Component } from "react";
import "./ClientHistory.css";

// class HistoryTable extends Component {
  export default ({ orders, showOrder}) => {

  orders.sort(function(a, b) {
    return -(a.id - b.id);
  });

        return(
          <div >
        <table >
            <tbody>
              {orders.map(order => {
                if (order.status === true) {
                  return (
                    <tr height="150" className="order">
                      <td className="info">
                        {/* шапка заказа */}
                        Заказ №: {order.id} <br />
                        Время заказа: {order.shipping_date}
                      </td>

                      <td>
                        {/* детали заказа */}
                        <button
                          className="details"
                          type="button"
                          onClick={() => showOrder(order)}
                        >
                          Посмотреть детали заказа..{" "}
                        </button>
                      </td>

                      <td className="cost">
                        Стоимость: <br />{" "}
                        <div className="digits-red">{order.amount}</div>
                      </td>
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
          </div>
        )
    
}
// export default HistoryTable