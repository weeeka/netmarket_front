import React from 'react';
import Category from "./Category";
import './Category.css';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Spinner from 'react-bootstrap/Spinner'
import ListGroup from 'react-bootstrap/ListGroup'
import Nav from 'react-bootstrap/Nav'


export default ({ data, update }) => {
    if (!data) {
        return <Spinner animation="border" variant="primary" />;
    }
    const categories = data.map((category, index) => {
        return (<Category category={category} index={category.id} key={`category-${index}`} update={update}/>);
    });

    categories.unshift(<Category category={{name: 'Все'}} index={null} key = {'categoryAll'} update={update}/>)

    return (
        <Nav className="flex-column">
            {categories}
        </Nav>
        
    );
}