import React from 'react';
import Button from 'react-bootstrap/Button'
import "./Category.css";
import ListGroup from 'react-bootstrap/ListGroup'
import Nav from 'react-bootstrap/Nav'

export default ({ category, update, index }) => {
    return (
        // <ListGroup.Item className="category-button" as="li" action onClick={() => update({ active: index })}>
        //     {category.name}
        // </ListGroup.Item>

        <Nav.Link  className="menu-item" onClick={() => update({ active: index })}>{category.name}</Nav.Link>
    );
};