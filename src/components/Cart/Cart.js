import React, { Component } from "react";
import { GetData } from "../../services/GetData";
import Button from "react-bootstrap/Button";
import { PayData } from "../../services/PayData";
import Modal from "react-bootstrap/Modal";
import CartList from "./CartList";
import { DeleteData } from "../../services/DeleteData";
import { PutData } from "../../services/PutData";
import { NavLink } from "react-router-dom";

import "./Cart.css";

class Cart extends Component {
  constructor() {
    super();
    this.state = {
      showModalToPay: false,
      showModalSuccess: false,
      order_id: 0,
      orderItems: [],
      sum: 0,
      clientStatus: 0
    };
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOpenModalToPay = this.handleOpenModalToPay.bind(this);
    this.handleCloseModalToPay = this.handleCloseModalToPay.bind(this);
    this.redirectToHome = this.redirectToHome.bind(this);
    this.pay = this.pay.bind(this);
    this.loadData = this.loadData.bind(this);
    this.loadData();
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
    document.location.href = "http://localhost:3000/home";
  }

  redirectToHome() {
    document.location.href = "http://localhost:3000/home";
  }

  handleOpenModalToPay() {
    this.setState({ showModalToPay: true });
  }

  handleCloseModalToPay() {
    this.setState({ showModalToPay: false });
  }

  loadData() {
    GetData("client/" + sessionStorage.getItem("username")).then(client => {
      GetData("order/" + client.id).then(order => {
        this.setState({
          order_id: order.id,
          sum: order.amount
        });
        GetData("orderItem/" + this.state.order_id).then(orderItems => {
          if (orderItems != null) {
            this.setState({
              orderItems: orderItems
            });
          }
        });
      });
      this.setState({
        clientStatus: client.bonus
      });
    });
  }

  pay() {
    PayData("pay/" + this.state.order_id)
      .then(order => {
        this.setState({
          showModalToPay: false,
          showModalSuccess: true
        });
      })
      .catch(error => {
        alert(error.message);
        console.log("aaaa");
      });
      
  }

  buttonTag() {
    if (this.state.sum <= 0 || this.state.sum == null) {
      return <div></div>;
    }
    if (this.state.clientStatus < this.state.sum) {
      return <Button disabled>Недостаточно средств</Button>;
    } else {
      return (
        <Button onClick={() => this.handleOpenModalToPay()}>Оплатить заказ</Button>
      );
    }
  }

  deleteAll() {
    let orderNew = {
      amount: 0,
      client_id: 0,
      getting_date: 0,
      shipping_date: 0,
      status: false
    };

    GetData("client/" + sessionStorage.getItem("username")).then(client => {
      GetData("order/" + client.id).then(order => {
        orderNew.amount = 0;
        orderNew.client_id = order.client_id;
        orderNew.getting_date = order.getting_date;
        orderNew.shipping_date = order.shipping_date;
        orderNew.status = order.status;
        GetData("orderItem").then(orderItem => {
          for (let i = 0; i < orderItem.length; i++) {
            if (orderItem[i].order_id === order.id) {
              DeleteData("orderItem/" + orderItem[i].id).then(orderItem1 => {
                PutData("order/" + order.id, orderNew)
                  .then(order1 => {
                    //this.loadData();
                  })
                  .then(() => {
                    this.loadData();
                  });
              });
            }
          }
        });
      });
    });
  }

  showCart() {
    if (this.state.orderItems.length != 0) {
      return (
        <div>

          <div className="cart-row-elems">

          <div>                     
            <CartList
              orderItems={this.state.orderItems}
              loadData={this.loadData}
            />
          </div>

          <div className="pay">
          
            <label className="pay-text">К оплате: {this.state.sum}</label>
            <br />
            {this.buttonTag()}
            <br />
            <br />
            <Button
              variant="secondary"
              size="sm"
              
              type="button"
              onClick={() => this.deleteAll()}
            >
              Очистить корзину
            </Button>
          
          </div>


          </div>


        </div>
      );
    } else{
      return(
        <>
        <div className="parent">
        <img className="pict" src="https://image.flaticon.com/icons/png/128/628/628551.png" />
      </div>
      <br />
      <div className="header-text parent">
        {" "}
        <span> </span>Корзина пуста :({" "}
      </div>
      <br />
      
      <div className="parent">
      <Button
        className="bbtn "
        variant="outline-dark"
        size="lg"
        onClick={() => this.redirectToHome()}
      >
        {" "}
        {/* <NavLink  to="home"> */}
        За покупками?{" "}
        {/* </NavLink> */}
      </Button> </div> </>
      )
    }
  }

  render() {
    return (
      <div style={{ margin: "5% 10%" }}>
        

        {this.showCart()}

        {/* модальные окна */}

        <Modal
          show={this.state.showModalToPay}
          onHide={this.handleCloseModalToPay}
          animation={true}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Списание средств</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            {this.state.sum} единиц будут{" "}
            <span className="warn-text">списаны</span> с твоего аккаунта
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModalToPay}>
              Закрыть
            </Button>
            <Button variant="primary" onClick={this.pay}>
              Оки
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={this.state.showModalSuccess}
          onHide={this.handleCloseModal}
          animation={true}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title></Modal.Title>
          </Modal.Header>

          <Modal.Body>Успешно</Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseModal}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
export default Cart;
