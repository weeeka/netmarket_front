import React, {Component} from 'react'
import {NavLink} from "react-router-dom";
import {GetData} from "../../services/GetData";
import {PayData} from "../../services/PayData";
import ReactModal from "react-modal";
import Button from "antd/es/button";
import {Label} from "react-foundation";


class  PayOrder extends Component {
    constructor() {
        super();
        this.state={
            showModal: false,
            amount: 0,
            id: null //order_id
        };
        this.loadData();
    }

    handleOpenModal(){
        this.setState({showModal: true});
    }

    handleCloseModal(){
        this.setState({showModal: false});
        document.location.href = "http://localhost:3000/home";
    }

    loadData() {
        GetData("client/" + sessionStorage.getItem("username")).then(client => {
            GetData("order/" + client.id).then(order => {
                this.setState({
                    id: order.id,
                    amount: order.amount
                });
            });
        });
    }

    pay(){
        PayData("pay/" + this.state.id)
            .then(order =>{
                this.setState({
                    showModal: true
                })
            })
            .catch(
                error => {
                    alert(error.message)
                });
    }

    render() {
        return(
            <div>
                <label>{this.state.amount} будут списаны с твоего аккаунта</label><br/>
                <Button  onClick={()=> this.pay()}>Ok</Button><br/>
                <NavLink to="/cart">Back</NavLink>

                <ReactModal isOpen = {this.state.showModal}>
                    <Label onMouseOver={()=> this.handleCloseModal()}>Заказ успешно выполнен</Label>
                </ReactModal>
            </div>)
    }
}
export default PayOrder;