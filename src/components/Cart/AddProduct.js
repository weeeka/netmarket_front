
import {GetData} from "../../services/GetData";
import {PostNData} from "../../services/PostNData";
import {PutData} from "../../services/PutData";
import ReactModal from "react-modal";
import React from "react";
import {Label} from "react-foundation";

export function Add(product, RunOutCallBack) {
    let showModal = false;
    let products = {
        order_id : null,
        price:  product.price,
        quantity: 1,
        product_id : product.id,
        name: product.name
    };
    let orderNew ={
        amount: 0,
        client_id: 0,
        getting_date: 0,
        shipping_date: 0,
        status: false
    };

    let state = false;

    GetData("client/" + sessionStorage.getItem("username")).then(client => {
        GetData("order/" + client.id).then(order => {
            GetData("orderItem/" + order.id).then(orderItems => {

                //если нет ордер_айтемов в этом заказе или нет еще пока ордер_айтема с таким продукт_айди 
                //или есть такой ордер_айтем в заказе и его количество не превосходит количество таких айтемов вообще
                if (orderItems.length == 0 || (orderItems.every( item => item.product_id != product.id )) || 
                orderItems.some( item => item.product_id == product.id && item.quantity < product.quantity))
                {
                            products.order_id = order.id;
                            orderNew.amount = order.amount + products.price;
                            orderNew.client_id = order.client_id;
                            orderNew.getting_date = order.getting_date;
                            orderNew.shipping_date = order.shipping_date;
                            orderNew.status = order.status;
                            PutData("order/"+ products.order_id, orderNew).then(order1=>{
                                PostNData("orderItem", products).then(orderItem=>{
                                    return(
                                        <ReactModal isOpen = {showModal}>
                                            <Label onMouseOver={()=> this.handleCloseModal()}>н</Label>
                                        </ReactModal>
                                    )
                                })
                            });
                } else state = true;
        }).then(()=>{RunOutCallBack(state)})
    });
});
}


