import React, { Component } from "react";
import {GetData} from "../../services/GetData";
import {DeleteData} from "../../services/DeleteData";
import {PutData} from "../../services/PutData";
import {PayData} from "../../services/PayData";
import Button from "react-bootstrap/Button";

// Необходимо импортировать и написанный нами ранее компонент UserData

class CartList extends Component {
    constructor(props) {
        super(props);

        //не работает, продукты в корзине при удалении перемещаются вниз
        this.props.orderItems.sort(function(a, b) {
            return a.id - b.id;
          });
      }
    //if (!data) { return (<p>Loading...</p>); }

    // const productsToBasket = data.map((productToBasket) => {
    //     return (<ProductToBasket productToBasket={productToBasket} />);
    // });

    delete(product) {
        let orderNew = {
            amount: 0,
            client_id: 0,
            getting_date: 0,
            shipping_date: 0,
            status: false
        };

        GetData("client/" + sessionStorage.getItem("username")).then(client => {
            GetData("order/" + client.id).then(order => {
                orderNew.amount = order.amount - product.price / product.quantity;
                orderNew.client_id = order.client_id;
                orderNew.getting_date = order.getting_date;
                orderNew.shipping_date = order.shipping_date;
                orderNew.status = order.status;

                GetData("orderItem").then(orderItem => {
                    for (let i = 0; i < orderItem.length; i++) {
                        if (orderItem[i].order_id === order.id && orderItem[i].product_id === product.product_id) {
                            if (orderItem[i].quantity === 1) {
                                DeleteData("orderItem/" + orderItem[i].id).then(orderItem2=>{
                                    PutData("order/" + order.id, orderNew).then(order1=>{
                                        //this.loadData();
                                    }).then(()=>{this.props.loadData()})
                                })
                            } else {
                                orderItem[i].price = orderItem[i].price - orderItem[i].price/orderItem[i].quantity;
                                orderItem[i].quantity = orderItem[i].quantity - 1;
                                PutData("orderItem/" + orderItem[i].id, orderItem[i]).then(orderItem2=> {
                                    PutData("order/" + order.id, orderNew).then(order1 => {
                                        //this.loadData();
                                    })
                                }).then(()=>{this.props.loadData()})
                            }
                            break;
                        }
                    }
                })
            });
        });
    }

    

    

    render() {
        return (<div>
        <table>
        <tbody>
          {this.props.orderItems.map(orderItem => {
            
              return (
                <tr height="150" className="order">
                  <td className="info">
                    {/* шапка заказа */}
                    Название: {orderItem.name} <br />
                    Количество: {orderItem.quantity}
                  </td>

                  <td>
                    {/* детали заказа */}
                    <button
                      className="details"
                      type="button"
                      onClick={() => this.delete(orderItem)}
                    >
                      Удалить из корзины{" "}
                    </button>
                  </td>

                  <td className="cost">
                    Цена: <br />{" "}
                    <div className="digits">{orderItem.price}</div>
                  </td>
                </tr>
              );
            
          })}
          <tr></tr>
        </tbody>
      </table>
        </div> 
    )
        }
} export default CartList