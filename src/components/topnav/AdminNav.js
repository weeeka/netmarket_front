import React, { Component } from "react";
import "./Topnav.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { NavLink } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";

class AdminNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTheme: "light"
    };

    this.logout = this.logout.bind(this);
    this.changeTheme = this.changeTheme.bind(this);
  }

  logout() {
    sessionStorage.clear();
  }

  changeTheme() {
    this.setState({
      currentTheme: this.state.currentTheme === "dark" ? "light" : "dark"
    });
    this.props.changeTheme(this.state.currentTheme)
  }

  //let currentTheme = 'light';

  render() {
    return (
      <div className="topnav">
        <Navbar
          expand="lg"
          bg={this.state.currentTheme}
          variant={this.state.currentTheme}
        >
          <Navbar.Brand>
            <NavLink to="/home">Netmarket</NavLink>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <NavLink to="/profile">
                <Nav.Link href="/profile">Профиль</Nav.Link>
              </NavLink>

              <NavLink to="/admin/clients">
                <Nav.Link href="/admin/clients">Банхамер</Nav.Link>
              </NavLink>

              <NavLink to="/admin/categories">
                <Nav.Link href="/admin/categories">Добавить категорию</Nav.Link>
              </NavLink>

              <NavLink to="/admin/products">
                <Nav.Link href="/admin/products">Товары</Nav.Link>
              </NavLink>

              <NavLink to="/admin/moneyClaims">
                <Nav.Link href="/admin/moneyClaims">Деньги</Nav.Link>
              </NavLink>

              <NavLink to="/admin/orders">
                <Nav.Link href="/admin/orders">Заказы</Nav.Link>
              </NavLink>

              <NavLink to="/info">
                <Nav.Link href="/info">Инфо</Nav.Link>
              </NavLink>
            </Nav>


            <Nav.Link>
              <NavLink to="/">
                <Button variant="info" onClick={this.logout}>
                  Выход
                </Button>
              </NavLink>
            </Nav.Link>
          
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
export default AdminNav;
