import React, { Component } from "react";
import "./Topnav.css";
import "bootstrap/dist/css/bootstrap.min.css";


import AdminNav from "./AdminNav";
import UserNav from "./UserNav";
import DefaultNav from "./DefaultNav";

class Topnav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: true,
      currentTheme: 'light'
    };

    this.logout = this.logout.bind(this);
    
  }

  changeTheme = (value) => {  
    this.setState({ currentTheme: value });
    this.props.updateData(this.state.currentTheme)
  }

  logout() {
    sessionStorage.clear();
    this.setState({ logged: false });
  }

  render() {
    if (sessionStorage.length !== 0) {
      if (sessionStorage.getItem('role') === '[ROLE_ADMIN]'){
          return (<AdminNav changeTheme={this.changeTheme}/>)
      } else return (<UserNav changeTheme={this.changeTheme}/>)
    } else return ( <DefaultNav changeTheme={this.changeTheme}/> )  
  }
}

export default Topnav;
