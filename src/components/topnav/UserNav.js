import React, { Component }  from "react";
import "./Topnav.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { NavLink } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";

class UserNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTheme: "light"
    };

    this.logout = this.logout.bind(this);
    this.changeTheme = this.changeTheme.bind(this);
  }

  logout() {
    sessionStorage.clear();
  }

  changeTheme() {
    this.setState({
      currentTheme: this.state.currentTheme === "dark" ? "light" : "dark"
    });
    this.props.changeTheme(this.state.currentTheme)
  } 

   logout() {
    sessionStorage.clear();
  }


  render() {
    return (
    <div className="top-nav ">
      <Navbar
          expand="lg"
          bg="light"
          variant="light"
          
        >
        <Navbar.Brand>
          <NavLink to="/home">Netmarket</NavLink>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavLink to="/cart">
              <Nav.Link href="/cart">Корзина</Nav.Link>
            </NavLink>
            <NavLink to="/profile">
              <Nav.Link href="/profile">Профиль</Nav.Link>
            </NavLink>
            <NavLink to="/info">
              <Nav.Link href="/info">Инфо</Nav.Link>
            </NavLink>
          </Nav>

        


          <Nav.Link>
            <NavLink to="/sale">
              <Button variant="success">Продать товар</Button>
            </NavLink>
          </Nav.Link>

          <Nav.Link>
            <NavLink to="/">
              <Button variant="info" onClick={this.logout}>
                Выход
              </Button>
            </NavLink>
          </Nav.Link>

          

        </Navbar.Collapse>
      </Navbar>
    </div>
  ); }
} export default UserNav;
