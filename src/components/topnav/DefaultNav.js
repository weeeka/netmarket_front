import React from "react";
import "./Topnav.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { NavLink } from "react-router-dom";
import { Redirect } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";

export default ({ update }) => {

  return (
    <div className="topnav">
      <Navbar expand="lg" bg="light" variant="light">
        <Navbar.Brand>
          <NavLink to="/">Netmarket</NavLink>
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavLink to="/info">
              <Nav.Link href="/info">Инфо</Nav.Link>
            </NavLink>
          </Nav>

          <Nav.Link>
            <NavLink to="/login">
              <Button variant="info">Вход</Button>
            </NavLink>
          </Nav.Link>
        </Navbar.Collapse>
        <Redirect to={"/"} />
      </Navbar>
    </div>
  );
};