import React, {Component} from "react";
import AdminProductList from "./AdminProduct/AdminProductList";
import {GetData} from "../../services/GetData";
import "./tablestyle.css"
import ReactPaginate from "react-paginate";


class ProductView extends Component{
    constructor() {
        super();
        this.state = {
            products: null,
            totalPages: null,
            currentNumPage: 0,
            pageSize: 10,
        };
        this.loadData();
        
        this.loadData = this.loadData.bind(this);

    }

    access(){
        if(sessionStorage.getItem('role') !== '[ROLE_ADMIN]') {
            this.props.history.push('/Home');
        }
    }
    componentDidMount(){
        this.access()
    }

    pageChange = (currentPage)=>{

        this.setState({
            currentNumPage: currentPage.selected}, () =>{
            this.loadData()
        });
    };
    loadData() {
        GetData("product?page="+this.state.currentNumPage+"&size="+this.state.pageSize).then(page => {
            this.setState({
                products: page.content,
                totalPages: page.totalPages
            });
        });
    }

    

    render() {
        return (
            <div style={{margin : '0 10rem'}}>              
                <div className="text-header">Все товары </div><br/><br/> 
                
                <AdminProductList data={this.state.products} loadData={this.loadData} />
                <ReactPaginate pageCount={this.state.totalPages}  pageRangeDisplayed={5}
                               marginPagesDisplayed={1} onPageChange={this.pageChange}
                               breakClassName={'page-item'}
                               breakLinkClassName={'page-link'}
                               containerClassName={'pagination'}
                               pageClassName={'page-item'}
                               pageLinkClassName={'page-link'}
                               previousClassName={'page-item'}
                               previousLinkClassName={'page-link'}
                               nextClassName={'page-item'}
                               nextLinkClassName={'page-link'}
                               activeClassName={'active'}
                />
            </div>
        );
    }


}export default ProductView