import React from 'react';
import "./ClientSearch.css";

export default ({ term, data, update }) => {

    const dataSearch = e => {
        const value = e.target.value.toLowerCase();

        const filter = data.filter(client => {
            return client.username.toLowerCase().includes(value);
        });

        update({
            clients: filter,
            term: value
        });

    };

    return (
        <div className="search-bar-container">
            <input
                value={term}
                type="text"
                className="form-control"
                placeholder="Search clent by name..."
                onChange={dataSearch}
            />
        </div>
    );
};