import React from "react";
import './Topnav.css';
import {NavLink} from "react-router-dom";
export default ({update})=>{

    function logout() {
        sessionStorage.removeItem('role');
        sessionStorage.removeItem('username')
        sessionStorage.removeItem('userToken');
        window.location.href= '/'
    }
    return (
        <div className="topnav">
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/admin/clients">Пользователи</NavLink>
            <NavLink to="/admin/categories">Категории</NavLink>
            <NavLink to="/admin/products">Продукты</NavLink>
            <NavLink to="/admin/moneyClaims">Заявки</NavLink>
            <input type="submit" className="button success" value="Logout" onClick={logout}/>
        </div>

    )

}



