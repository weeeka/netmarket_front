import React, {Component} from "react";
import {GetData} from "../../services/GetData";
import AddCategory from "./AddCategory/AddCategory";
import CategoryList from "../categories/CategoryList";


class CategoryView extends Component{
    constructor() {
        super();
        this.state = {
            categories: null,
        };
        this.loadData()

    }

    access(){
        if(sessionStorage.getItem('role') !== '[ROLE_ADMIN]') {
            this.props.history.push('/Home');
        }
    }
    componentDidMount(){
        this.access()
    }

    loadData() {
        GetData('category').then(categories => {
            this.setState({
                categories: categories
            });
        });}

    render() {
        return (
            <div>
                
                <AddCategory/>
                <CategoryList data={this.state.categories}/>

            </div>
        );
    }


}export default CategoryView