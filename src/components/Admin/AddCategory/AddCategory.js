import React, {Component} from 'react'
import {Label} from "react-foundation";
import Button from "antd/es/button";
import {PostData} from "../../../services/PostData";

class AddCategory extends Component {
    constructor() {
        super();
        this.state={
            name: '',
        }
        this.save = this.save.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }


    save(){
        PostData('category',this.state );
    }




    render() {
        return(
            <div>
                <Label>Введите категорию:</Label><br/>
                <input type="text" name="name" placeholder="Категория" onChange={this.onChange}/><br/>
                <Button onClick={() => this.save()}>Save </Button><br/>
            </div>)
    }
}
export default AddCategory;