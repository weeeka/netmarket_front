import React from 'react';
import "../tablestyle.css";

import AdminProduct from "./AdminProduct";


export default ({ data, loadData}) => {
    if (!data) { return (<p>Loading...</p>); }

    // function compare(a, b) {
    //     return a.id - b.id;
    //   }

    //все последние добавленные товары отображает в начале списка
    data.sort(function(a, b) {
        return -(a.id - b.id);
      });

    const products = data.map((product) => {
        return (<AdminProduct product={product} loadData={loadData}/>);
    })
    return (
        <table class="table">
      <tbody>{products}</tbody>
    </table>
    );
};