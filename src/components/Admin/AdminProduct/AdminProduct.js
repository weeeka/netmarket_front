import React, {Component} from "react";
import { PutData } from "../../../services/PutData";
import Button from "react-bootstrap/Button";
import {GetData} from "../../../services/GetData";
import "./product.css";

// export default ({ product, loadData }) =>
 class AdminProduct extends Component{

  constructor(props) {
    super(props);
    this.state = {
        name:""      
    };
    this.showSellerName(this.props.product);

    this.buttonTag = this.buttonTag.bind(this);
    this.hide = this.hide.bind(this);
    this.show = this.show.bind(this);

}

  hide(product) {
    
    product.valide = false;
    PutData("product/" + product.id, product).then(()=>{this.props.loadData()})
     
  }

  show(product) {
    
    product.valide = true;
    PutData("product/" + product.id, product).then(()=>{this.props.loadData()})
     
  }

  showSellerName(product) {
    GetData('client/findById/' + product.client_id).then(client => {     
        this.setState({
            name: client.username
        });   
      })
  }

  buttonTag() {
    if (this.props.product.valide) {
      return (
        <Button
          className="butn"
          variant="primary"
          size="sm"
          onClick={() => this.hide(this.props.product)}
        >
          Убрать с сайта
        </Button>
      ); 
    } else {
      return (
        <Button
          className="butn"
          variant="secondary"
          size="sm"
          onClick={() => this.show(this.props.product)}
        >
          Выложить товар на сайт
        </Button>
      );
    } 
  }

  render()
  {
  return (
    <tr>
      <div class="card" class="table">
        <div class="card-body">
          <h5 class="card-title">{this.props.product.name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">Цена: {this.props.product.price}</h6>
  <h6 class="card-subtitle mb-2 text-muted">Выложил пользователь {this.state.name}</h6>
          <p class="card-text">В наличии: {this.props.product.quantity}</p>
          <p class="card-text">Код игры: <span className="code">{this.props.product.code}</span></p>
          <div>{this.buttonTag()}</div>
        </div>
      </div>
    </tr>
  )
}
} export default AdminProduct
