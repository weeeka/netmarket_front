import React, {Component} from "react";

import {GetData} from "../../services/GetData";
import MoneyClaimList from "./AdminMoney/MoneyClaimList";

import "./tablestyle.css"

class MoneyClaimView extends Component{
    constructor() {
        super();
        this.state = {
            moneyClaims: null,
        };
        this.loadData();
        this.loadData = this.loadData.bind(this);
    }

    access(){
        if(sessionStorage.getItem('role') !== '[ROLE_ADMIN]') {
            this.props.history.push('/Home');
        }
    }
    componentDidMount(){
        this.access()
    }

    loadData() {
        GetData('moneyClaim').then(moneyClaims => {
            this.setState({
                moneyClaims: moneyClaims
            });
        });}

    render() {
        return (
            <div style={{margin : '2rem 10rem'}}>
                <div className="text-header">Запросы на пополнение баланса </div><br/><br/>
                <MoneyClaimList data={this.state.moneyClaims} loadData={this.loadData}/>

            </div>
        );
    }


}export default MoneyClaimView