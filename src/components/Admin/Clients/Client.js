import React from 'react';
import {PutData} from "../../../services/PutData";
import Button from "react-bootstrap/Button";
import "../tablestyle.css";

export default ({ client, loadData}) => {

    function ban(client){
        client.enabled = false
        PutData('client/'+ client.username, client).then(()=>{loadData()})
    }

    function unban(client){
      client.enabled = true
        PutData('client/'+ client.username, client).then(()=>{loadData()})
    }

    //false - забанен
    function buttonTag() {
      if (client.enabled) {
        return (
          <Button
            className="butn"
            variant="primary"
            size="sm"
            onClick={() => ban(client)}
          >
            Забанить
          </Button>
        ); 
      } else {
        return (
          <Button
            className="butn"
            variant="secondary"
            size="sm"
            onClick={() => unban(client)}
          >
            Разбанить
          </Button>
        );
      } 
    }

    return (
        <tr>
            {/* <td></td>
             */}
            <div class="card" class="table">
            <div class="card-body">
              <h5 class="card-title">{client.username}</h5>
              <h6 class="card-subtitle mb-2 text-muted">email</h6>
              <p class="card-text">some info</p>
              <div>{buttonTag()}</div>
            </div>
          </div>
        </tr>
    );
};