import React from 'react';
import Client from "./Client";
import "../tablestyle.css";

export default ({ data, loadData }) => {
    if (!data) { return (<p>Loading...</p>); }

    data.sort(function(a, b) {
        return a.id - b.id;
      });

      
    const clients = data.map((client) => {
        return (<Client client={client} loadData={loadData}/>);
    });
    return (
        <table class="table">
            {/* <thead>
            <tr>
                <th>Name</th>

            </tr>
            </thead> */}

            <tbody>
            {clients}
            </tbody>
        </table>
    );
};