import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

class OrderModal extends Component {
  render() {
    return (
        <Modal show={this.props.showModal} onHide={this.props.handleCloseModal} animation={true}  centered>

        <Modal.Header closeButton>
          < Modal.Title>Состав заказа</Modal.Title>
        </Modal.Header>

      <Modal.Body>    
      <div className="window">Покупатель {this.props.clientName}  </div> <br/>
      <div className="window">   
      <table className="orderItem-list">
                        <thead>
                        <tr>
                            <th>Название продукта</th>
                            <th>Цена</th>
                            <th>Количество</th>                                
                        </tr>
                        </thead>

                        <tbody>
                        
                        {this.props.orderItems.map((productToOrder) => {
                            return (
                                <tr>
                                    <td>{productToOrder.name}</td>
                                    <td>{productToOrder.price}</td>
                                    <td>{productToOrder.quantity}</td>
                                </tr>);
                        })}
                        </tbody>
                    </table>
                    </div>
      </Modal.Body>


      <Modal.Footer>
        <Button variant="secondary" onClick={this.props.handleCloseModal}>
          Закрыть
        </Button>
      </Modal.Footer>

    </Modal>
    );
  }
}

export default OrderModal;