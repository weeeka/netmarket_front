import React, { Component } from "react";

class HistoryTable extends Component {
  render() {
    return (
      <table>
        <tbody>
          {this.props.orders.map(order => {
            if (order.status === true) {
              return (
                <tr height="150" className="order">
                  <td className="info">
                    {/* шапка заказа */}
                    Заказ №: {order.id} <br />
                    Время заказа: {order.shipping_date}
                  </td>

                  <td>
                    {/* детали заказа */}
                    <button
                      className="details"
                      type="button"
                      onClick={() => this.props.showOrder(order)}
                    >
                      Посмотреть детали заказа..{" "}
                    </button>
                  </td>

                  <td className="cost">
                    Стоимость: <br />{" "}
                    <div className="digits">{order.amount}</div>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    );
  }
}

export default HistoryTable;
