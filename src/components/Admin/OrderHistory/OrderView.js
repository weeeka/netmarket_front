import React, { Component } from "react";
import { GetData } from "../../../services/GetData";
import "./orders.css";

import HistoryTable from "./HistoryTable";
import OrderModal from "./OrderModal";

class OrderView extends Component {
  constructor() {
    super();
    this.state = {
      orders: [],
      orderItems: [],
      showModal: false,
      clientName: ""
    };
    this.loadData();
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  access() {
    if (sessionStorage.getItem("role") !== "[ROLE_ADMIN]") {
      this.props.history.push("/Home");
    }
  }
  componentDidMount() {
    this.access();
  }

  updateData(config) {
    this.setState(config);
  }

  loadData() {
    GetData("order").then(orders => {
      this.setState({
        orders: orders
      });
    });
  }

  showOrder = (order) => {
    GetData("orderItem/" + order.id).then(orderItems => {
      this.setState({
        orderItems: orderItems,
        showModal: true
      });
    });
    GetData("client/findById/" + order.client_id).then(client => {
      this.setState({
        clientName:
          client.lastName + " " + client.firstName + " " + "@" + client.username
      });
    });
  }

  render() {
    //console.log(this.state.orders.length)
    return (
      <div style={{ margin: "0 10rem" }}>
        <br />
        <div className="text-header">Заказы всех пользователей</div>
        <br />
        <br />

        <HistoryTable orders={this.state.orders} showOrder={this.showOrder} />
        <OrderModal
          showModal={this.state.showModal}
          clientName={this.state.clientName}
          orderItems={this.state.orderItems}
          handleCloseModal={this.handleCloseModal}
        />
      </div>
    );
  }
}
export default OrderView;
