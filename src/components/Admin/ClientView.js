import React, {Component} from "react";
import {GetData} from "../../services/GetData";
import ClientList from "./Clients/ClientList";
import ClientSearch from "./ClientSearch/ClientSearch";

import "./tablestyle.css"

class ClientView extends Component{
    constructor() {
        super();
        this.state = {
            clients: null,
            term: '',

        };
        this.loadData();
        this.loadData = this.loadData.bind(this);
    }
    access(){
        if(sessionStorage.getItem('role') !== '[ROLE_ADMIN]') {
            this.props.history.push('/Home');
        }
    }
    componentDidMount(){
        this.access()
    }

    updateData(config) {
        this.setState(config);
    }

    loadData() {
        GetData('client').then(clients => {
            this.initialClients = clients
            this.setState({
                clients: clients
            });
        });
    }



    render() {
        return (
            <div style={{margin : '2rem 10rem'}}>
               
                <ClientSearch update={this.updateData.bind(this)} term={this.state.term} data={this.initialClients}/>
                <div className="text-header">Пользователи</div>
                <ClientList data = {this.state.clients} loadData= {this.loadData}/>

            </div>
        );
    }


}export default ClientView