import React from "react";
import MoneyClaim from "./MoneyClaim";
import "../tablestyle.css";

export default ({ data,loadData }) => {
  if (!data) {
    return <p>Loading...</p>;
  }
  const moneyClaims = data.map(moneyClaim => {
    return <MoneyClaim moneyClaim={moneyClaim} loadData={loadData}/>;
  });
  return (
    <table class="table">
      <tbody>{moneyClaims}</tbody>
    </table>
  );
};
