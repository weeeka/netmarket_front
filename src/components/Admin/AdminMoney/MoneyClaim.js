import {PutData} from "../../../services/PutData";
import React from "react";
import Button from "react-bootstrap/Button";

export default ({ moneyClaim, loadData}) => {
    function review(moneyClaim, decision) {
        loadData()
        PutData('moneyClaim/review?'+'moneyClaimId='+moneyClaim.id+'&decision='+decision+'&visibleAmount='+moneyClaim.amount, ).then(()=>{loadData()}).catch(()=>{})
    }

    //wait for acceptance - ожидает получить подтверждение
    //запрос на деньги будет показан только если админ его не принял и не отклонил
    function waitForAcpt(){
      if (moneyClaim.status == null)
        return (
          <tr>           
            <div class="card" class="table">
            <div class="card-body">
              <h5 class="card-title">{moneyClaim.amount} бонусов</h5>
              <h6 class="card-subtitle mb-2 text-muted">Клиент: {moneyClaim.clientId}</h6>
              {/* <p class="card-text">Статус: {moneyClaim.status}</p>   */}
              <Button  size="sm" variant="info" onClick={()=> review(moneyClaim, true)}> Подтверждаем  </Button>
              <Button  size="sm" variant="secondary" onClick={()=> review(moneyClaim, false)}>  Отклоняем</Button>         
            </div>
          </div>

        </tr>
        )
        else return (<div></div>)
    }

    return (
        <>{waitForAcpt()}</>
    );
};