import React, {Component} from 'react';
import { Link} from 'react-router-dom';
import {PostData} from '../../services/PostData';
import './Login.css';

class Login extends Component {

  

  constructor(){
    super();
    this.state = {
     username: '',
     password: '',
     //enabled: false
    };

    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);

  }

  

  login() {
    if(this.state.username && this.state.password){
      PostData('login',this.state).then((result) => {
       let responseJson = result;
       //console.log(result);
       if(responseJson){             
         sessionStorage.setItem('userToken',responseJson.access_token);
         sessionStorage.setItem('username', this.state.username);
         sessionStorage.setItem('role', responseJson.role)
         this.props.history.push('/home');
         console.log(sessionStorage.getItem('userToken'));
        //this.setState({enabled: true});
       }
       
      }, error => {alert("Неправильный логин или пароль ");});
    }
   }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
   }

   render() {
    return (
      <div className="content">
      
        
          <h4>Login</h4>
          <label>Username</label>
          <input
            type="text"
            name="username"
            placeholder="Username"
            onChange={this.onChange}
          />
          <label>Password</label>
          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={this.onChange}
          />
          <input
            type="submit"
            className="button info"
            value="Login"
            onClick={this.login}
          />
          <Link to="/registration">Registration</Link>
        
      
      </div>
      
    );
  }
}

export default Login;