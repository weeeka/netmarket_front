import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Button from "react-bootstrap/Button";

import "./Welcome.css";

class Welcome extends Component {
  render() {
    if (sessionStorage.length !== 0) 
      return (
        <div className="content">
          <h1 className="header">Netmarket</h1>
        </div>
      );
     else {
      return (
        <div className="content">
          <h1 className="header">Netmarket</h1>
          <p className="text">Sample sample sample text</p>
          <NavLink to="/login">
            <Button variant="info" className="btn">
              Вход
            </Button>
          </NavLink>
          <NavLink to="/registration">
            <Button variant="light" className="btn">
              Регистрация
            </Button>
          </NavLink>
        </div>
      );
    }
  }
}

export default Welcome;
