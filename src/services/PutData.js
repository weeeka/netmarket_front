
export function PutData(type, Data) {
    let BaseURL = 'http://localhost:8080/';

    return new Promise((resolve, reject) =>{
        fetch(BaseURL+type, {
            method: 'PUT',
            headers: {"Authorization": "Bearer"+" " + sessionStorage.getItem('userToken'), "Content-Type": "application/json"},
            body: JSON.stringify(Data)
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });
    });
}