export function PostData(type, userData) {
    let BaseURL = 'http://localhost:8080/';

    return new Promise((resolve, reject) =>{
        const headers = new Headers({
            'Content-Type': 'application/json',
        })

        if(sessionStorage.getItem('userToken')) {
            headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('userToken'))
        }
        fetch(BaseURL+type, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(userData)
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });

  
      });
}












