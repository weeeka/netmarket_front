export function PostNData(type, userData) {
    let BaseURL = 'http://localhost:8080/';

    return new Promise((resolve, reject) =>{
        fetch(BaseURL+type, {
            method: 'POST',
            headers: {"Authorization": "Bearer"+" " + sessionStorage.getItem('userToken'),
                "Content-Type": "application/json"},
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });


    });
}
