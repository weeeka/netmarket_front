import React, { Component } from "react";

import "./styles/foundation.min.css";
import "./styles/custom.css";
import Routes from "./routes";
import Topnav from "./components/topnav/Topnav.js"
import "./App.css"


class App extends Component {

  constructor() {
    super();
    this.state = {
      currentTheme: 'light'
    };}

  updateData = (value) => {  this.setState({ currentTheme: value })}

  render() {
    return (  
      <div className={this.state.currentTheme}>
      <Topnav className="topnav" updateData={this.updateData}/>                  
      <Routes />
      </div>     
    )
  }
}

export default App;
