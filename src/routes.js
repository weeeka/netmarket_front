import React from 'react';
import {Route,  Switch} from 'react-router-dom';

import Welcome from '././components/Welcome/Welcome';
import Login from '././components/Login/Login';
import SignUp from "./components/SignUp/SignUp";
import Home from "./components/Home/Home";
import Cart from "./components/Cart/Cart";
import Profile from "./components/Profile/Profile";
import ProfileEdit from "./components/Profile/ProfileEdit";
import ClientView from "./components/Admin/ClientView";
import CategoryView from "./components/Admin/CategoryView";
import ProductView from "./components/Admin/ProductView";
import PayOrder from "./components/Cart/PayOrder";
import Ex from "./components/Ex"
import Sale from "./components/Sale/Sale";

import Info from "./components/Info/Info";
import MoneyClaimView from "./components/Admin/MoneyClaimView";
import OrderView from "./components/Admin/OrderHistory/OrderView";
import ClientHistory from "./components/ClientHistory/ClientHistory";



const Routes = () => (
  
      <Switch>
          <Route exact path="/" component={Welcome}/>
          <Route path="/login" component={Login}/>
          <Route path="/registration" component={SignUp}/>
          <Route path="/home" component={Home}/>
          <Route path="/cart" component={Cart}/>
          <Route path="/profile" component={Profile}/>
          <Route path="/profileEdit" component={ProfileEdit}/>
          <Route path="/admin/clients" component={ClientView}/>
          <Route path="/admin/categories" component={CategoryView}/>
          <Route path="/admin/products" component={ProductView}/>
          <Route path="/admin/moneyClaims" component={MoneyClaimView}/>
          <Route path="/pay_order" component={PayOrder}/>
          <Route path="/modal" component={Ex}/>
          <Route path="/sale" component={Sale}/>
          <Route path="/info" component={Info}/>
          <Route path="/admin/orders" component={OrderView}/>
          <Route path="/history" component={ClientHistory}/>
      </Switch>
);

export default Routes;